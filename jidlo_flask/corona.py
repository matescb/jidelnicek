import pandas as pd
import plotly
import plotly.graph_objects as go


def sumusa(df):
    df["USA"]=df[['US — Adams, IN',
 'US — Alachua, FL',
 'US — Alameda County, CA',
 'US — Alaska',
 'US — Anoka, MN',
 'US — Arapahoe, CO',
 'US — Arizona',
 'US — Arkansas',
 'US — Arlington, VA',
 'US — Beadle, SD',
 'US — Bennington County, VT',
 'US — Bergen County, NJ',
 'US — Berkshire County, MA',
 'US — Bernalillo, NM',
 'US — Bon Homme, SD',
 'US — Boone, IN',
 'US — Broward County, FL',
 'US — Bucks, PA',
 'US — Burlington, NJ',
 'US — Calaveras, CA',
 'US — California',
 'US — Camden, NC',
 'US — Camden, NJ',
 'US — Carver County, MN',
 'US — Charles Mix, SD',
 'US — Charleston County, SC',
 'US — Charlotte County, FL',
 'US — Charlton, GA',
 'US — Chatham County, NC',
 'US — Cherokee County, GA',
 'US — Clark County, NV',
 'US — Clark County, WA',
 'US — Cobb County, GA',
 'US — Collier, FL',
 'US — Collin County, TX',
 'US — Colorado',
 'US — Connecticut',
 'US — Contra Costa County, CA',
 'US — Cook County, IL',
 'US — Cuyahoga, OH',
 'US — Dallas, TX',
 'US — Dane, WI',
 'US — Davidson County, TN',
 'US — Davis County, UT',
 'US — Davison, SD',
 'US — DeKalb, GA',
 'US — Delaware',
 'US — Delaware County, PA',
 'US — Denver County, CO',
 'US — Deschutes, OR',
 'US — Diamond Princess',
 'US — District of Columbia',
 'US — Douglas County, CO',
 'US — Douglas County, NE',
 'US — Douglas County, OR',
 'US — Eagle, CO',
 'US — El Paso County, CO',
 'US — Essex, MA',
 'US — Fairfax County, VA',
 'US — Fairfield, CT',
 'US — Fayette County, KY',
 'US — Fayette, GA',
 'US — Florida',
 'US — Floyd, GA',
 'US — Fort Bend County, TX',
 'US — Fresno County, CA',
 'US — Fulton County, GA',
 'US — Georgia',
 'US — Grafton County, NH',
 'US — Grand Princess',
 'US — Grant County, WA',
 'US — Gregg, TX',
 'US — Gunnison, CO',
 'US — Gwinnett, GA',
 'US — Hanover, VA',
 'US — Harford County, MD',
 'US — Harris County, TX',
 'US — Harrison County, KY',
 'US — Hawaii',
 'US — Hendricks County, IN',
 'US — Hillsborough, FL',
 'US — Honolulu County, HI',
 'US — Howard, IN',
 'US — Hudson County, NJ',
 'US — Humboldt County, CA',
 'US — Idaho',
 'US — Illinois',
 'US — Indiana',
 'US — Iowa',
 'US — Island, WA',
 'US — Jackson County, OR ',
 'US — Jefferson County, KY',
 'US — Jefferson County, WA',
 'US — Jefferson Parish, LA',
 'US — Jefferson, CO',
 'US — Johnson County, IA',
 'US — Johnson County, KS',
 'US — Johnson, IN',
 'US — Kane, IL',
 'US — Kansas',
 'US — Kentucky',
 'US — Kershaw County, SC',
 'US — King County, WA',
 'US — Kitsap, WA',
 'US — Kittitas County, WA',
 'US — Klamath County, OR',
 'US — Knox, NE',
 'US — Lake, IL',
 'US — Lancaster, SC',
 'US — Larimer, CO',
 'US — Lee County, FL',
 'US — Litchfield, CT',
 'US — Los Angeles, CA',
 'US — Loudoun, VA',
 'US — Louisiana',
 'US — Madera County, CA',
 'US — Maine',
 'US — Manatee County, FL',
 'US — Maricopa County, AZ',
 'US — Marin, CA',
 'US — Marion County, IN',
 'US — Marion County, OR',
 'US — Maryland',
 'US — Massachusetts',
 'US — McHenry, IL',
 'US — Michigan',
 'US — Middlesex County, MA',
 'US — Middlesex, NJ',
 'US — Minnehaha, SD',
 'US — Minnesota',
 'US — Mississippi',
 'US — Missouri',
 'US — Monmouth, NJ',
 'US — Monroe, PA',
 'US — Montana',
 'US — Montgomery County, MD',
 'US — Montgomery County, PA',
 'US — Montgomery, TX',
 'US — Multnomah, OR',
 'US — Napa, CA',
 'US — Nassau County, NY',
 'US — Nassau, FL',
 'US — Nebraska',
 'US — Nevada',
 'US — New Castle, DE',
 'US — New Hampshire',
 'US — New Jersey',
 'US — New Mexico',
 'US — New York',
 'US — New York County, NY',
 'US — Noble, IN',
 'US — Norfolk County, MA',
 'US — Norfolk, VA',
 'US — North Carolina',
 'US — North Dakota',
 'US — Oakland, MI',
 'US — Ohio',
 'US — Okaloosa County, FL',
 'US — Oklahoma',
 'US — Olmsted, MN',
 'US — Orange County, CA',
 'US — Oregon',
 'US — Orleans, LA',
 'US — Pasco, FL',
 'US — Passaic, NJ',
 'US — Pennington, SD',
 'US — Pennsylvania',
 'US — Philadelphia, PA',
 'US — Pierce County, WA',
 'US — Pierce, WI',
 'US — Pima, AZ',
 'US — Pinal County, AZ',
 'US — Pinellas, FL',
 'US — Placer County, CA',
 'US — Polk County, GA',
 'US — Polk, OR',
 'US — Pottawattamie, IA',
 "US — Prince George's, MD",
 'US — Providence County, RI',
 'US — Ramsey County, MN',
 'US — Rhode Island',
 'US — Riverside County, CA',
 'US — Rockingham County, NH',
 'US — Rockland County, NY',
 'US — Sacramento County, CA',
 'US — San Benito, CA',
 'US — San Diego County, CA',
 'US — San Francisco County, CA',
 'US — San Joaquin, CA',
 'US — San Mateo, CA',
 'US — Santa Clara County, CA',
 'US — Santa Cruz, CA',
 'US — Santa Rosa County, FL',
 'US — Saratoga County, NY',
 'US — Shasta County, CA',
 'US — Shelby County, TN',
 'US — Skagit, WA',
 'US — Snohomish County, WA',
 'US — Socorro, NM',
 'US — Solano, CA',
 'US — Sonoma County, CA',
 'US — South Carolina',
 'US — South Dakota',
 'US — Spartanburg County, SC',
 'US — Spotsylvania, VA',
 'US — St. Joseph, IN',
 'US — St. Louis County, MO',
 'US — Stanislaus, CA',
 'US — Stark, OH',
 'US — Suffolk County, MA',
 'US — Suffolk County, NY',
 'US — Sullivan, TN',
 'US — Summit County, CO',
 'US — Summit, UT',
 'US — Tarrant, TX',
 'US — Tennessee',
 'US — Texas',
 'US — Thurston, WA',
 'US — Tulsa County, OK',
 'US — Ulster County, NY',
 'US — Umatilla, OR',
 'US — Union, NJ',
 'US — Utah',
 'US — Ventura, CA',
 'US — Vermont',
 'US — Virginia',
 'US — Volusia County, FL',
 'US — Wake County, NC',
 'US — Washington',
 'US — Washington County, OR',
 'US — Washington, D.C.',
 'US — Washoe County, NV',
 'US — Wayne County, PA',
 'US — Wayne, MI',
 'US — Weber, UT',
 'US — West Virginia',
 'US — Westchester County, NY',
 'US — Whatcom, WA',
 'US — Williamson County, TN',
 'US — Wisconsin',
 'US — Worcester, MA',
 'US — Wyoming',
 'US — Yolo County, CA',]].sum(axis=1)
    return(df)


# In[2]:


def norm(df):
    df2=pd.read_csv("population-figures-by-country-csv_csv.csv",sep=",")[["Country","Country_Code","Year_2016"]]#.set_index("no")
    df2=df2.set_index("Country_Code")
    df2.loc["CZE"]
    
    dfn=pd.DataFrame()
    dfn["US"]=df["US"]/df2.loc["USA","Year_2016"]*1000000
    dfn["Czechia"]=df["Czechia"]/df2.loc["CZE","Year_2016"]*1000000
    dfn["Italy"]=df["Italy"]/df2.loc["ITA","Year_2016"]*1000000

    dfn["France"]=df["France"]/df2.loc["FRA","Year_2016"]*1000000
    dfn["Germany"]=df["Germany"]/df2.loc["DEU","Year_2016"]*1000000
    dfn["Spain"]=df["Spain"]/df2.loc["ESP","Year_2016"]*1000000
    dfn["Poland"]=df["Poland"]/df2.loc["POL","Year_2016"]*1000000
    dfn["Slovakia"]=df["Slovakia"]/df2.loc["SVK","Year_2016"]*1000000
    dfn["Sweden"]=df["Sweden"]/df2.loc["SWE","Year_2016"]*1000000
    dfn["Bulgaria"]=df["Bulgaria"]/df2.loc["BGR","Year_2016"]*1000000
    dfn["United Kingdom"]=df["United Kingdom"]/df2.loc["GBR","Year_2016"]*1000000
    
    dfn["Austria"]=df["Austria"]/df2.loc["AUT","Year_2016"]*1000000
    dfn["New Zealand"]=df["New Zealand"]/df2.loc["NZL","Year_2016"]*1000000
    dfn["Norway"]=df["Norway"]/df2.loc["NOR","Year_2016"]*1000000
    
    return(dfn)


# In[3]:


def coroplot():
    df=pd.read_csv("https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv",sep=",")#.set_index("no")
    #df=pd.read_csv("https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_19-covid-Deaths.csv",sep=",")#.set_index("no")

    zeme=["Italy","Czechia","US","France","Germany","Spain","Poland","Slovakia","Sweden","Bulgaria","United Kingdom","Austria",'New Zealand','Norway']

    df.loc[df["Province/State"].notna(),"Place"]=df[df["Province/State"].notna()]["Country/Region"]+ " — " + df[df["Province/State"].notna()]["Province/State"]
    df.loc[df["Province/State"].isna(),"Place"]=df[df["Province/State"].isna()]["Country/Region"]

    df=df[df["Country/Region"]!="Mainland China"]

    df=df.T
    df.columns=df.loc["Place"]
    df=df.drop(["Long","Lat","Province/State","Country/Region","Place"], axis=0)
    #df=sumusa(df)

    dfn=norm(df)

    # vykresli 
    fig = go.Figure()

    '''
    for a in zeme:
        fig.add_trace(go.Scatter(x=list(df.index), y=df[a],
                            mode='lines+markers',
                            name=a))
    plotly.offline.plot(fig,filename='corona.html',show_link=False, auto_open=True)

    fig = go.Figure()
    for a in zeme:
        fig.add_trace(go.Scatter(x=list(df.index), y=df[a].diff(),
                            mode='lines+markers',
                            name=a))
    plotly.offline.plot(fig,filename='corona_diff.html',show_link=False, auto_open=True)

    fig = go.Figure()
    for a in zeme:
        fig.add_trace(go.Scatter(x=list(dfn.index), y=dfn[a],
                            mode='lines+markers',
                            name=a))
    plotly.offline.plot(fig,filename='corona_norm.html',show_link=False, auto_open=True)



    fig = go.Figure()

    for a in ["Italy","Czech Republic","France","Germany","South Korea","US — Los Angeles, CA","US — San Diego County, CA","Hong Kong — Hong Kong"]:
        fig.add_trace(go.Scatter(x=list(df.index), y=df[a],
                            fill='tozeroy',
                        mode="none",
                            name=a,stackgroup='one'))


    plotly.offline.plot(fig,filename='corona.html',show_link=False, auto_open=True)
'''


    for a in zeme:
        fig.add_trace(go.Scatter(x=list(dfn.index), y=dfn[a],
                            mode='lines+markers',
                            name=a))
        
        fig.update_layout(
    autosize=False,
    width=1200,
    height=800,)
    
    return plotly.offline.plot(fig, 
                                    config={"displayModeBar": False}, 
                                    show_link=False, 
                                    include_plotlyjs=False, 
                                    output_type='div')