from datetime import datetime
from jidlo_flask import db, ma, login_manager
from flask_login import UserMixin

from sqlalchemy.orm import relationship,backref

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))

class User(db.Model, UserMixin):
    id=db.Column(db.Integer,primary_key=True)
    username=db.Column(db.String(20),unique=True,nullable=False)
    email=db.Column(db.String(120),unique=True,nullable=False)
    image_file=db.Column(db.String(20),nullable=False,default="default.jpg")
    password=db.Column(db.String(60),nullable=False)

    posts=db.relationship('Post', backref='author', lazy=True)

    def __repr__(self):
        return f"User('{self.username}','{self.email}','{self.image_file}')"


class Post(db.Model):
    id=db.Column(db.Integer,primary_key=True)
    title=db.Column(db.String(100),nullable=False)
    date_posted=db.Column(db.DateTime,nullable=False,default=datetime.utcnow)
    content = db.Column(db.Text, nullable=False)

    user_id=db.Column(db.Integer,db.ForeignKey('user.id'),nullable=False)

    def __repr__(self):
        return f"Post('{self.title}','{self.date_posted}','{self.id}')"

class Ingred(db.Model):
    __tablename__ = 'ingreds'
    id=db.Column(db.Integer,primary_key=True)
    name=db.Column(db.String(30),nullable=False)
    energy=db.Column(db.Integer,nullable=True)
    prot=db.Column(db.Integer,nullable=True)
    carb=db.Column(db.Integer,nullable=True)
    fat=db.Column(db.Integer,nullable=True)

    recipes = relationship("Recipe", secondary="link_ingreds")

class IngredSchema(ma.Schema):
    class Meta:
        fields = ("id","name", "energy", "prot", "carb", "fat")

class Recipe(db.Model):
    __tablename__ = 'recipes'
    id=db.Column(db.Integer,primary_key=True)
    name=db.Column(db.String(30),nullable=False)
    notes=db.Column(db.Text,nullable=True)
    fuel=db.Column(db.String(30),nullable=True)

    ingreds = relationship("Ingred", secondary="link_ingreds")
    days = relationship("Day", secondary="link_meals")
    

class LinkIngred(db.Model):
    __tablename__ = 'link_ingreds'
    id=db.Column(db.Integer,primary_key=True)
    ingred_id=db.Column(db.Integer,db.ForeignKey("ingreds.id"),nullable=False)
    recipe_id=db.Column(db.Integer,db.ForeignKey("recipes.id"),nullable=False)

    amount=db.Column(db.Integer,nullable=False,default=0)

    ingred = relationship(Ingred, backref=backref("link_ingreds", cascade="all, delete-orphan"))
    recipe = relationship(Recipe, backref=backref("link_ingreds", cascade="all, delete-orphan"))

class Day(db.Model):
    __tablename__ = 'days'
    id=db.Column(db.Integer,primary_key=True)
    name=db.Column(db.String(30),nullable=True)
    notes=db.Column(db.Text,nullable=True)
    energy_required=db.Column(db.Integer,nullable=True,default=0)
    kilometers=db.Column(db.Integer,nullable=True,default=0)
    elevation=db.Column(db.Integer,nullable=True,default=0)

    recipes = relationship("Recipe", secondary="link_meals")
    cookies = relationship("Cookie", secondary="link_cookies")


class LinkMeal(db.Model):
    __tablename__ = 'link_meals'
    id=db.Column(db.Integer,primary_key=True)
    day_id=db.Column(db.Integer,db.ForeignKey("days.id"),nullable=False)
    recipe_id=db.Column(db.Integer,db.ForeignKey("recipes.id"),nullable=False)

    energy=db.Column(db.Integer,nullable=False,default=0)
    meal_type=db.Column(db.String(20),nullable=False,default=0)
    
    days = relationship(Day, backref=backref("link_meals", cascade="all, delete-orphan"))
    recipes = relationship(Recipe, backref=backref("link_meals", cascade="all, delete-orphan"))

class Diet(db.Model):
    __tablename__ = 'diets'
    id=db.Column(db.Integer,primary_key=True)
    name=db.Column(db.String(30),nullable=False)
    notes=db.Column(db.Text,nullable=False,default="")
    energy_required=db.Column(db.Integer,nullable=False,default=0)
    number_of_days=db.Column(db.Integer,nullable=False,default=0)
    kilometers=db.Column(db.Integer,nullable=False,default=0)
    elevation=db.Column(db.Integer,nullable=False,default=0)
    people=db.Column(db.Integer,nullable=False,default=0)

    days = relationship("Day", secondary="link_diets")


class LinkDiet(db.Model):
    __tablename__ = 'link_diets'
    id=db.Column(db.Integer,primary_key=True)
    day_id=db.Column(db.Integer,db.ForeignKey("days.id"),nullable=False)
    diet_id=db.Column(db.Integer,db.ForeignKey("diets.id"),nullable=False)

class Cookie(db.Model):
    __tablename__ = 'cookies'
    id=db.Column(db.Integer,primary_key=True)
    name=db.Column(db.String(30),nullable=False)
    energy=db.Column(db.Integer,nullable=False,default=0)
    prot=db.Column(db.Integer,nullable=False,default=0)
    carb=db.Column(db.Integer,nullable=False,default=0)
    fat=db.Column(db.Integer,nullable=False,default=0)

    weight=db.Column(db.Integer,nullable=True,default=0)

    days = relationship("Day", secondary="link_cookies")
    
class LinkCookies(db.Model):
    __tablename__ = 'link_cookies'
    id=db.Column(db.Integer,primary_key=True)
    day_id=db.Column(db.Integer,db.ForeignKey("days.id"),nullable=False)
    cookie_id=db.Column(db.Integer,db.ForeignKey("cookies.id"),nullable=False)

    count=db.Column(db.Integer,nullable=True,default=1)

    cookie = relationship(Cookie, backref=backref("link_cookies", cascade="all, delete-orphan"))
    day = relationship(Day, backref=backref("link_cookies", cascade="all, delete-orphan"))


class CookieSchema(ma.Schema):
    class Meta:
        fields = ("id","name", "energy", "prot", "carb", "fat", "weight")

class CookieListSchema(ma.Schema):
    class Meta:
        fields = ("id","name")
    
class LinkCookiesSchema(ma.Schema):
    class Meta:
        fields = ("id","cookie","count")
    cookie = ma.Nested(CookieSchema())


class LinkIngredSchema(ma.Schema):
    class Meta:
        fields = ("id","ingred","amount")
    ingred = ma.Nested(IngredSchema())

class RecipeSchema(ma.Schema):
    class Meta:
        fields = ("id","name","notes", "fuel","link_ingreds")
    link_ingreds = ma.Nested(LinkIngredSchema(many=True))

class RecipeListSchema(ma.Schema):
    class Meta:
        fields = ("id","name")

class LinkMealSchema(ma.Schema):
    class Meta:
        fields = ("id","ingred","energy","mealt_type", "recipes")
    recipes = ma.Nested(RecipeSchema())

class DaySchema(ma.Schema):
    class Meta:
        fields = ("link_meals","link_cookies","id","name","notes", "energy_required","kilometers","elevation")
    link_meals = ma.Nested(LinkMealSchema(many=True))
    link_cookies = ma.Nested(LinkCookiesSchema(many=True))

class DietSchema(ma.Schema):
    class Meta:
        fields = ("id","name","notes", "energy_required","days","kilometers","elevation","people")
    days = ma.Nested(DaySchema(many=True))       

class DietListSchema(ma.Schema):
    class Meta:
        fields = ("id","name")
    #days_link = relationship(Day, backref=backref("link_diets", cascade="all, delete-orphan"))
    #diets = relationship(Diet, backref=backref("link_diets", cascade="all, delete-orphan"))

