
def makronut_sum_relative(recipe):
    sums={"energy":0,"amount":0,"carb":0,"fat":0,"prot":0,}
    output={}
    for link in recipe['link_ingreds']:
        sums["energy"]+=link['ingred']['energy']*link['amount']/100
        sums["amount"]+=link['amount']
        sums["carb"]+=link['ingred']['carb']
        sums["fat"]+=link['ingred']['fat']
        sums["prot"]+=link['ingred']['prot']

    if sums["amount"]>0:
        recipe["absolute_energy"]=round(sums["energy"])
        recipe["absolute_amount"]=round(sums["amount"])
    
        recipe["relative_energy"]=round(sums["energy"]/sums["amount"]*100)
        
        totalMakro=sums["carb"]+sums["fat"]+sums["prot"]

        if totalMakro>0:
            recipe["relative_carb"]=round(sums["carb"]/totalMakro*100,1)
            recipe["relative_fat"]=round(sums["fat"]/totalMakro*100,1)
            recipe["relative_prot"]=round(sums["prot"]/totalMakro*100,1)
        else:
            recipe["relative_carb"]=0
            recipe["relative_fat"]=0
            recipe["relative_prot"]=0
    else:
        recipe["relative_carb"]=0
        recipe["relative_fat"]=0
        recipe["relative_prot"]=0       
        recipe["absolute_energy"]=0
        recipe["absolute_amount"]=0
        recipe["relative_energy"]=0
    return recipe


def makronut_set_energy(recipe, energy):
    output={}
    sums={"energy":0,"amount":0}
    for link in recipe['link_ingreds']:
        if sums["amount"]>0:
            link['amount']=round(link['amount']*energy/recipe['absolute_energy'])
        else:
            link['amount']=0

        sums["energy"]+=link['ingred']['energy']*link['amount']/100
        sums["amount"]+=link['amount']

    if sums["amount"]>0:
        recipe["relative_energy"]=round(sums["energy"]/sums["amount"]*100)
    else:
        recipe["relative_energy"]=0
    
    recipe["absolute_energy"]=round(sums["energy"])
    recipe["absolute_amount"]=round(sums["amount"])

    return recipe

def CalToJ(energy):
    return round(energy*4.18400)
 
def JToCal(energy):
    return round(energy*0.239005736)


   