from flask import render_template, url_for, flash, redirect, request, jsonify
from jidlo_flask.forms import RegistrationForm,LoginForm, PostForm
from jidlo_flask import app,db, bcrypt

from jidlo_flask.models import User, Post, Ingred, Recipe, LinkIngred, IngredSchema, LinkIngredSchema,RecipeSchema,RecipeListSchema
from jidlo_flask.models import DietListSchema, DietSchema, Diet,Cookie, CookieListSchema, CookieSchema
from jidlo_flask.models import Day, DaySchema, LinkMeal, LinkCookies

from flask_login import login_user, current_user, logout_user, login_required

from jidlo_flask.corona import coroplot

from jidlo_flask.functions_mat import makronut_sum_relative, makronut_set_energy, CalToJ, JToCal

@app.route('/')
@app.route('/home')
def home():
    posts=Post.query.all()
    return render_template("main.html",posts=posts, title="python title")

@app.route('/about')
def about():
    return render_template("about.html", title="python title")

@app.route('/register', methods=['GET','POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = RegistrationForm()

    if form.validate_on_submit():
        hashed_password=bcrypt.generate_password_hash(form.password.data).decode("utf-8")
        user=User(username=form.username.data,email=form.email.data,password=hashed_password)
        db.session.add(user)
        db.session.commit()
        flash(f'Account created for {form.username.data}! You can login now.',"success")
        return redirect(url_for("login"))
    return render_template("register.html", title="Register",form=form)

@app.route('/login', methods=['GET','POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form=LoginForm()
    if form.validate_on_submit():
        user=User.query.filter_by(username=form.username.data).first()
        print(bcrypt.check_password_hash(user.password, form.password.data))
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            login_user(user, remember=form.remember.data)
            next_page=request.args.get("next")

            return redirect(next_page) if next_page else redirect(url_for("home"))
        else:
            flash(f'FAIL!',"danger")   
    return render_template("login.html", title="Login",form=form)

@app.route("/logout")
def logout():
    logout_user()
    return redirect(url_for('home'))


@app.route("/account")
@login_required
def account():
    
    return render_template("account.html", title="Account")


@app.route("/corona")
@login_required
def corona():
    return render_template("corona.html", title="Corona", corona=coroplot())

@app.route("/post/new", methods=['GET','POST'])
@login_required
def new_post():
    form=PostForm()

    if form.validate_on_submit():
        post=Post(title=form.title.data, content=form.content.data, author=current_user)
        db.session.add(post)
        db.session.commit()
        flash(f'Post was created',"success")
        return redirect(url_for("home"))


    return render_template("create_post.html", title="New post", form=form)

@app.route("/suroviny")
#@login_required
def suroviny():
    return render_template("suroviny.html", title="Corona", suroviny=Ingred.query.all())


@app.route("/ingreds", methods=['GET', 'POST'])
def ingreds():
    response_object = {'status': 'success'}
    if request.method == 'POST':
        post_data = request.get_json()
        ingredience=Ingred(
            name=post_data.get('name'),
            energy=post_data.get('energy'),
            prot=post_data.get('prot'),
            carb=post_data.get('carb'),
            fat=post_data.get('fat'))

        db.session.add(ingredience)
        db.session.commit()

        response_object['message'] = 'Ingredience přidána!'
    else:    
        ingred_schema = IngredSchema()
        ingreds_schema = IngredSchema(many=True)
        
        ingreds = Ingred.query.all()
        result=ingreds_schema.dump(ingreds)
        
        response_object['ingredients'] = result
    return jsonify(response_object)

@app.route('/ingreds/<ingred_id>', methods=['PUT', 'DELETE'])
def single_book(ingred_id):
    response_object = {'status': 'success'}
    if request.method == 'PUT':
        post_data = request.get_json()

        ingred=Ingred.query.filter_by(id=ingred_id).first()

        ingred.name=post_data.get('name')
        ingred.energy=post_data.get('energy')
        ingred.prot=post_data.get('prot')
        ingred.carb=post_data.get('carb')
        ingred.fat=post_data.get('fat')
        db.session.commit()

        response_object['message'] = 'Ingredience upravena!'
    if request.method == 'DELETE':
        Ingred.query.filter_by(id=ingred_id).delete()
        db.session.commit()
        response_object['message'] = 'Ingredience smazana!'
    return jsonify(response_object)

@app.route("/recipe", methods=['GET', 'POST','PUT', 'DELETE'])
def recipe():
    response_object = {'status': 'success'}

    if request.method == 'POST':
        post_data = request.get_json()
        recipe=Recipe(
            name=post_data.get('name'),
            fuel=post_data.get('fuel'),
            notes=post_data.get('notes'))

        db.session.add(recipe)
        db.session.commit()

    if request.method == 'PUT':
        post_data = request.get_json()
        recipe=Recipe.query.filter_by(id=post_data.get('recipe_id')).first()
        recipe.name=post_data.get('name')
        recipe.fuel=post_data.get('fuel')
        recipe.notes=post_data.get('notes')

        db.session.commit()
        response_object['message'] = 'Recept upraven!'

    if request.method == 'DELETE':
        post_data = request.get_json()
        print(post_data)
        Recipe.query.filter_by(id=post_data.get('recipe_id')).delete()
        db.session.commit()
        response_object['message'] = 'Recept smazan!'

    if request.method == 'GET':

        if 'recipe_id' in request.args:
            recipe_id = request.args.get('recipe_id', default = -1, type = int)
            recipe_schema = RecipeSchema()
            recipe = Recipe.query.get(recipe_id)
            result=recipe_schema.dump(recipe)       
           # result=makronut_sum_relative(result)
            if 'set_energy' in request.args:
                result=makronut_set_energy(result,(request.args.get('set_energy', type = int)))
            response_object['recipe'] = result

        if 'list' in request.args:
            recipe_schema = RecipeListSchema(many=True)
            recipe = Recipe.query.all()
            result=recipe_schema.dump(recipe)               
            response_object['recipelist'] = result

    return jsonify(response_object)


@app.route("/diet", methods=['GET', 'POST','PUT', 'DELETE'])
def diet():
    response_object = {'status': 'success'}

    if request.method == 'POST':
        post_data = request.get_json()
        diet=Diet(
            name=post_data.get('name'),
            fuel=post_data.get('fuel'),
            notes=post_data.get('notes'),
            energy_required=post_data.get('energy_required'),
            number_of_days=post_data.get('number_of_days'),
            kilometers=post_data.get('kilometers'),
            elevation=post_data.get('elevation'),
            people=post_data.get('people'))

        db.session.add(diet)
        db.session.commit()
        response_object['message'] = 'Jídelníček přidán!'

    if request.method == 'PUT':
        post_data = request.get_json()
        diet=Diet.query.filter_by(id=post_data.get('diet_id')).first()
  
        diet.name=post_data.get('name'),
        diet.fuel=post_data.get('fuel'),
        diet.notes=post_data.get('notes'),
        diet.energy_required=post_data.get('energy_required'),
        diet.number_of_days=post_data.get('number_of_days'),
        diet.kilometers=post_data.get('kilometers'),
        diet.elevation=post_data.get('elevation'),
        diet.people=post_data.get('people')

        db.session.commit()
        response_object['message'] = 'Jídelníček upraven!'

    if request.method == 'DELETE':
        post_data = request.get_json()
        Diet.query.filter_by(id=post_data.get('diet_id')).delete()
        db.session.commit()
        response_object['message'] = 'Jídelníček smazan!'

    if request.method == 'GET':

        if 'diet_id' in request.args:
            diet_id = request.args.get('diet_id', default = -1, type = int)
            diet_schema = DietSchema()
            diet = Diet.query.get(diet_id)
            result=diet_schema.dump(diet)       

            response_object['diet'] = result

        if 'list' in request.args:
            diets_schema = DietListSchema(many=True)
            diet = Diet.query.all()
            result=diets_schema.dump(diet)               
            response_object['dietlist'] = result

    return jsonify(response_object)


@app.route("/link", methods=['POST','PUT','DELETE'])
def link():
    response_object = {'status': 'success'}
    if request.method == 'POST':
        post_data = request.get_json()

        link=LinkIngred(ingred_id=post_data.get('ingred_id'),
                  recipe_id=post_data.get('recipe_id'),
                  amount=post_data.get('amount'))
        
        db.session.add(link)
        db.session.commit()
        response_object['message'] = 'Ingredience přidána!'

    if request.method == 'PUT':
        post_data = request.get_json()
        LinkIngred.query.filter_by(id=post_data.get('link_id')).first().amount=post_data.get('amount')
        
        db.session.commit()
        response_object['message'] = 'Ingredience updatovana!'

    if request.method == 'DELETE':
        post_data = request.get_json()
        LinkIngred.query.filter_by(id=post_data.get('link_id')).delete()
        db.session.commit()
        response_object['message'] = 'Ingredience smazana!'

    return jsonify(response_object)


@app.route("/cookie", methods=['GET', 'POST','PUT', 'DELETE'])
def cookie():
    response_object = {'status': 'success'}

    if request.method == 'POST':
        post_data = request.get_json()
        cookie=Cookie(
            name=post_data.get('name'),
            energy=post_data.get('energy'),
            prot=post_data.get('prot'),
            carb=post_data.get('carb'),
            fat=post_data.get('fat'))

        db.session.add(cookie)
        db.session.commit()
        response_object['message'] = 'Sušenka přidána!'

    if request.method == 'PUT':
        post_data = request.get_json()
        cookie=Cookie.query.filter_by(id=post_data.get('id')).first()
        cookie.name=post_data.get('name')
        cookie.energy=post_data.get('energy')
        cookie.prot=post_data.get('prot')
        cookie.carb=post_data.get('carb')
        cookie.fat=post_data.get('fat')

        db.session.commit()
        response_object['message'] = 'Sušenka upravena!'

    if request.method == 'DELETE':
        post_data = request.get_json()
        print(post_data)
        Cookie.query.filter_by(id=post_data.get('id')).delete()
        db.session.commit()
        response_object['message'] = 'Sušenka smazana!'

    if request.method == 'GET':

        if 'id' in request.args:
            id = request.args.get('id', default = -1, type = int)
            cookie_schema = CookieSchema()
            cookie = Cookie.query.get(id)
            result=cookie_schema.dump(cookie)       
            response_object['cookie'] = result

        if 'list' in request.args:
            cookie_schema = CookieListSchema(many=True)
            cookie = Cookie.query.all()
            result=cookie_schema.dump(cookie)               
            response_object['cookielist'] = result

    return jsonify(response_object)



#This is TBD
@app.route("/day", methods=['GET', 'POST','PUT', 'DELETE'])
def day():
    response_object = {'status': 'success'}

    if request.method == 'POST':
        if 'day' in request.args:
            post_data = request.get_json()
            day=Day(
                name=post_data.get('name'),
                notes=post_data.get('notes'),
                energy_required=post_data.get('energy_required'),
                kilometers=post_data.get('kilometers'),
                elevation=post_data.get('elevation'))

            diet=Diet.query.filter_by(id=1).first()
            diet.days.append(day)    

            db.session.commit()
            response_object['message'] = 'Den přidán!'

        if "meals" in request.args:
            post_data = request.get_json()
            day_id = request.args.get('day_id', default = -1, type = int)
            for meal in post_data:
                day=Day.query.filter_by(id=day_id).first()
                day.recipes.append(Recipe.query.filter_by(id=meal["id"]).first())
                db.session.commit()
            response_object['message'] = 'OK!'

        if "cookies" in request.args:
            post_data = request.get_json()
            day_id = request.args.get('day_id', default = -1, type = int)
            for cookie in post_data:
                day=Day.query.filter_by(id=day_id).first()
                day.cookies.append(Cookie.query.filter_by(id=cookie["id"]).first())
                db.session.commit()
            response_object['message'] = 'OK!'

    if request.method == 'PUT':
        if 'day' in request.args:
            post_data = request.get_json()
            day=Day.query.filter_by(id=post_data.get('id')).first()
            post_data = request.get_json()
            day.name=post_data.get('name')
            day.notes=post_data.get('notes')
            day.energy_required=post_data.get('energy_required')
            day.kilometers=post_data.get('kilometers')
            day.elevation=post_data.get('elevation')

            db.session.commit()
            response_object['message'] = 'OK!'

        if 'cookieLink' in request.args:
            id = request.args.get('cookieLink', default = -1, type = int)
            post_data = request.get_json()
            cookie=LinkCookies.query.filter_by(id=id).first()
            cookie.count=post_data.get('count')

            db.session.commit()
            response_object['message'] = 'OK!'

        if 'mealLink' in request.args:
            id = request.args.get('mealLink', default = -1, type = int)
            post_data = request.get_json()
            meal=LinkMeal.query.filter_by(id=id).first()
            meal.energy=post_data.get('energy')

            db.session.commit()
            response_object['message'] = 'OK!'

    if request.method == 'DELETE':
        if 'id' in request.args:
            id = request.args.get('id', default = -1, type = int)
            post_data = request.get_json()
            print(post_data)
            LinkMeal.query.filter_by(day_id=id).delete()
            LinkCookies.query.filter_by(day_id=id).delete()
            Day.query.filter_by(id=id).delete()
            db.session.commit()
            response_object['message'] = 'Den smazan!'
        
        if "recipe_id" in request.args:
            post_data = request.get_json()
            day_id = request.args.get('day_id', default = -1, type = int)
            recipe_id = request.args.get('recipe_id', default = -1, type = int)
            LinkMeal.query.filter_by(day_id=day_id, recipe_id=recipe_id).delete()
            db.session.commit()
            response_object['message'] = 'OK!'     

        if "cookie_id" in request.args:
            post_data = request.get_json()
            day_id = request.args.get('day_id', default = -1, type = int)
            cookie_id = request.args.get('cookie_id', default = -1, type = int)
            LinkCookies.query.filter_by(day_id=day_id, cookie_id=cookie_id).delete()
            db.session.commit()
            response_object['message'] = 'OK!'

    if request.method == 'GET':
        '''     
        if 'id' in request.args:
            id = request.args.get('id', default = -1, type = int)
            cookie_schema = CookieSchema()
            cookie = Cookie.query.get(id)
            result=cookie_schema.dump(cookie)       
            response_object['cookie'] = result
        '''
        response_object['message'] = 'TBD!'

    return jsonify(response_object)