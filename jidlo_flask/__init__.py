from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_login import LoginManager
from flask_marshmallow import Marshmallow

from flask_migrate import Migrate

from flask_cors import CORS

app = Flask(__name__)
app.config["SECRET_KEY"]='c7974a23e0f7967ab9523765d500e9ad269c518847859bc493'
app.config["SQLALCHEMY_DATABASE_URI"]='sqlite:///site_new.db'
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"]=False

db=SQLAlchemy(app)

migrate = Migrate(app, db)

ma = Marshmallow(app)

bcrypt=Bcrypt(app)
login_manager=LoginManager(app)
login_manager.login_view="login"
login_manager.login_message_category='info'

# enable CORS
CORS(app, resources={r'/*': {'origins': '*'}})

from jidlo_flask import routes