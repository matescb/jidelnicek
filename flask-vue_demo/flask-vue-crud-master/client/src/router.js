import Vue from 'vue';
import Router from 'vue-router';
import Ingred from './components/Ingred.vue';
import Recipe from './components/Recipe.vue';
import Ping from './components/Ping.vue';
import Test from './components/Test.vue';
import Home from './components/Home.vue';
import Diet from './components/diet/Diet.vue';

import HelloWorld from './components/HelloWorld.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
    },    
    {
      path: '/test',
      name: 'Test',
      component: Test,
    },
    {
      path: '/recipe',
      name: 'Recipe',
      component: Recipe,
    },
    {
      path: '/diet',
      name: 'Diet',
      component: Diet,
    },
  ],
});
