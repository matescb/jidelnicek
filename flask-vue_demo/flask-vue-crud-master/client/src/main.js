import 'bootstrap/dist/css/bootstrap.css';
import BootstrapVue from 'bootstrap-vue';
import Vue from 'vue';

import axios from 'axios';

export const eventBus = new Vue({
  
  data () {
  return {
    dietID:1,
    diet: [],

    recipelist: [],   
    cookielist: [],

    }
},
  methods: {
    getDiet() {
      const path = `http://localhost:5000/diet?diet_id=${this.dietID}`;
      axios.get(path)
        .then((res) => {
          this.diet = res.data.diet;
        })
        .catch((error) => {
          // eslint-disable-next-line
          console.error(error);
        });
    }, 

    getRecipeList() {
      const path = `http://localhost:5000/recipe?list`;
      axios.get(path)
        .then((res) => {
          this.recipelist = res.data.recipelist;
          console.info(this.recipelist)
        })
        .catch((error) => {
          // eslint-disable-next-line
          console.error(error);
        });
    },
    getCookieList() {
      const path = `http://localhost:5000/cookie?list`;
      axios.get(path)
        .then((res) => {
          this.cookielist = res.data.cookielist;
          console.info(this.cookielist)
        })
        .catch((error) => {
          // eslint-disable-next-line
          console.error(error);
        });
    },
  },
  created() {
    this.getRecipeList();
    this.getCookieList();
    this.getDiet();
  },
});

import App from './App.vue';
import router from './router';

import recipe from './components/Recipe.vue';
import ingred from './components/Ingred.vue';
import diet from './components/diet/Diet.vue';


Vue.use(BootstrapVue);

Vue.component('recipe', recipe);
Vue.component('ingred', ingred);
Vue.component('diet', diet);

Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App),
}).$mount('#app');
