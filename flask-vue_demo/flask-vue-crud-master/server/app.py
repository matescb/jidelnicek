import uuid

from flask import Flask, jsonify, request
from flask_cors import CORS


BOOKS = [
    {
        'id': uuid.uuid4().hex,
        'title': 'On the Road',
        'author': 'Jack Kerouac',
        'read': True
    },
    {
        'id': uuid.uuid4().hex,
        'title': 'Harry Potter and the Philosopher\'s Stone',
        'author': 'J. K. Rowling',
        'read': False
    },
    {
        'id': uuid.uuid4().hex,
        'title': 'Green Eggs and Ham',
        'author': 'Dr. Seuss',
        'read': True
    }
]

INGRED = [
    {
        'id': uuid.uuid4().hex,
        'name': 'Sul',
        'energy': '1000',
        'prot': '10',
        'carb': '20',
        'fat': '30'
    },    {
        'id': uuid.uuid4().hex,
        'name': 'pepr',
        'energy': '1000',
        'prot': '10',
        'carb': '20',
        'fat': '30'
    },    {
        'id': uuid.uuid4().hex,
        'name': 'cukr',
        'energy': '1000',
        'prot': '10',
        'carb': '20',
        'fat': '30'
    }
]

# configuration
DEBUG = True

# instantiate the app
app = Flask(__name__)
app.config.from_object(__name__)

# enable CORS
CORS(app, resources={r'/*': {'origins': '*'}})


def remove_book(book_id):
    for book in BOOKS:
        if book['id'] == book_id:
            BOOKS.remove(book)
            return True
    return False

def remove_ingred(ingred_id):
    for ingred in INGRED:
        if ingred['id'] == ingred_id:
            INGRED.remove(ingred)
            return True
    return False


# sanity check route
@app.route('/ping', methods=['GET'])
def ping_pong():
    return jsonify('pong!')




@app.route('/ingreds/<indred_id>', methods=['PUT', 'DELETE'])
def single_book(indred_id):
    response_object = {'status': 'success'}
    if request.method == 'PUT':
        post_data = request.get_json()
        remove_ingred(indred_id)
        INGRED.append({
            'id': uuid.uuid4().hex,
            'name': post_data.get('name'),
            'energy': post_data.get('energy'),
            'prot': post_data.get('prot'),
            'carb': post_data.get('carb'),
            'fat': post_data.get('fat')
        })
        response_object['message'] = 'Ingredience upravena!'
    if request.method == 'DELETE':
        remove_ingred(indred_id)
        response_object['message'] = 'Ingredience upravena!'
    return jsonify(response_object)

@app.route('/ingreds', methods=['GET', 'POST'])
def all_ingreds():
    response_object = {'status': 'success'}
    if request.method == 'POST':
        post_data = request.get_json()
        INGRED.append({
            'id': uuid.uuid4().hex,
            'name': post_data.get('name'),
            'energy': post_data.get('energy'),
            'prot': post_data.get('prot'),
            'carb': post_data.get('carb'),
            'fat': post_data.get('fat')
        })
        response_object['message'] = 'Ingredience upravena!'
    else:
        response_object['ingredients'] = INGRED
    return jsonify(response_object)  


if __name__ == '__main__':
    app.run()